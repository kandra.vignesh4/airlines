import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookingService } from '../../services/booking.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html'
})
export class BookingComponent implements OnInit {
  bookingForm: FormGroup;
  bookings: any[] = [];

  constructor(
    private fb: FormBuilder,
    private bookingService: BookingService
  ) {
    this.bookingForm = this.fb.group({
      origin: ['', Validators.required],
      destination: ['', Validators.required],
      date: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadBookings();
  }

  onSubmit(): void {
    if (this.bookingForm.valid) {
      this.bookingService.bookFlight(this.bookingForm.value).subscribe(
        success => {
          if (success) {
            alert('Booking successful');
            this.loadBookings();
          } else {
            alert('Booking failed');
          }
        }
      );
    }
  }

  loadBookings(): void {
    this.bookingService.getBookings().subscribe(
      bookings => {
        this.bookings = bookings;
      }
    );
  }

  deleteBooking(index: number): void {
    this.bookingService.deleteBooking(index).subscribe(
      success => {
        if (success) {
          this.loadBookings();
        } else {
          alert('Failed to delete booking');
        }
      }
    );
  }
}
