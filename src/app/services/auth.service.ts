import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(credentials: any): Observable<boolean> {
    const storedUser = localStorage.getItem(credentials.email);
    if (storedUser) {
      const user = JSON.parse(storedUser);
      if (user.password === credentials.password) {
        return of(true);
      }
    }
    return of(false);
  }

  register(data: any): Observable<boolean> {
    if (localStorage.getItem(data.email)) {
      return of(false); // User already exists
    }
    localStorage.setItem(data.email, JSON.stringify(data));
    return of(true);
  }
}
