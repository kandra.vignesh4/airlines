import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor() { }

  bookFlight(data: any): Observable<boolean> {
    let bookings = JSON.parse(localStorage.getItem('bookings') || '[]');
    bookings.push(data);
    localStorage.setItem('bookings', JSON.stringify(bookings));
    return of(true);
  }

  getBookings(): Observable<any[]> {
    const bookings = JSON.parse(localStorage.getItem('bookings') || '[]');
    return of(bookings);
  }

  deleteBooking(index: number): Observable<boolean> {
    let bookings = JSON.parse(localStorage.getItem('bookings') || '[]');
    if (index > -1 && index < bookings.length) {
      bookings.splice(index, 1);
      localStorage.setItem('bookings', JSON.stringify(bookings));
      return of(true);
    }
    return of(false);
  }
}
